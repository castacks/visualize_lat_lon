#ifndef LATLONVISUALIZER_H
#define LATLONVISUALIZER_H
#include "ros/ros.h"
#include "visualization_msgs/Marker.h"
#include "tf_utils/tf_utils.h"
#include "local_frame_manager/local_frame_manager.h"
#include "vector"
#include "dem_reader/dem.h"

class LatLonVisualizer
{    
    std::vector<geometry_msgs::Point> pvector_;
    std::string frame_;
    std::string pub_topic;
    std::string ns_;
    std::vector<std_msgs::ColorRGBA> cvector_;
    double scale_;
    ros::Publisher marker_pub_;
    DEMData dem_reader_;

public:
    LocalFrameManager lfm_client_;
    LatLonVisualizer(){}
    bool Initialize(ros::NodeHandle& n);
    bool PutLatLonPointsOnGround();
    void PublishMarker();
};

#endif // LATLONVISUALIZER_H
