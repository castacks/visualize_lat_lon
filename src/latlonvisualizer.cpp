#include "visualize_lat_lon/latlonvisualizer.h"

bool LatLonVisualizer::Initialize(ros::NodeHandle &n){
    bool server = false;
    if(!lfm_client_.Initialize(server,n)){
        ROS_ERROR_STREAM("Could not initialize server.");
        return false;
    }

    if (!n.getParam("pub_topic", pub_topic)){
      ROS_ERROR_STREAM("Could not get pub_topic parameter.");
      return false;
    }
    marker_pub_ = n.advertise<visualization_msgs::Marker>
            (pub_topic, 1);

    if (!n.getParam("namespace", ns_)){
      ROS_ERROR_STREAM("Could not get namespace parameter.");
      return false;
    }

    if (!n.getParam("scale", scale_)){
      ROS_ERROR_STREAM("Could not get scale parameter.");
      return false;
    }

    if (!n.getParam("frame", frame_)){
      ROS_ERROR_STREAM("Could not get frame parameter.");
      return false;
    }


    int num_obj;
    if (!n.getParam("number_of_objects", num_obj)){
      ROS_ERROR_STREAM("Could not get number_of_objects parameter.");
      return false;
    }
    if(num_obj==0){
        ROS_ERROR_STREAM("Zero Objects, exiting");
        return false;
    }

    std::string lat_string="lat_obj";
    std::string lon_string="lon_obj";
    std::string color_string = "color_obj";
    for(size_t i=0; i<num_obj; i++){
        std::string temp_lat_string = lat_string;
        std::string temp_lon_string = lon_string;
        {
            std::stringstream ss; ss << i;
            temp_lat_string.append(ss.str());
            temp_lon_string.append(ss.str());
        }
        geometry_msgs::Point p;
        if (!n.getParam(temp_lat_string, p.x)){
          ROS_ERROR_STREAM("Could not get "<< temp_lat_string<<".");
          return false;
        }
        if (!n.getParam(temp_lon_string, p.y)){
          ROS_ERROR_STREAM("Could not get "<< temp_lon_string<<".");
          return false;
        }

        pvector_.push_back(p);

        std_msgs::ColorRGBA c;
        {
            std::stringstream ss; ss << i; ss << "_r";
            std::string temp_color_string = color_string;
            temp_color_string.append(ss.str());
            if (!n.getParam(temp_color_string, c.r)){
              ROS_ERROR_STREAM("Could not get "<< temp_color_string<<".");
              return false;
            }
        }

        {
            std::stringstream ss; ss << i; ss << "_b";
            std::string temp_color_string = color_string;
            temp_color_string.append(ss.str());
            if (!n.getParam(temp_color_string, c.b)){
              ROS_ERROR_STREAM("Could not get "<< temp_color_string<<".");
              return false;
            }
        }


        {
            std::stringstream ss; ss << i; ss << "_g";
            std::string temp_color_string = color_string;
            temp_color_string.append(ss.str());
            if (!n.getParam(temp_color_string, c.g)){
              ROS_ERROR_STREAM("Could not get "<< temp_color_string<<".");
              return false;
            }
        }

        {
            std::stringstream ss; ss << i; ss << "_a";
            std::string temp_color_string = color_string;
            temp_color_string.append(ss.str());
            if (!n.getParam(temp_color_string, c.a)){
              ROS_ERROR_STREAM("Could not get "<< temp_color_string<<".");
              return false;
            }
        }

        cvector_.push_back(c);
    }

    //set up dem file
    std::string dem_file;
    if (!n.getParam("dem_file", dem_file)){
      ROS_ERROR_STREAM("Could not get dem_file parameter.");
      return false;
    }

    if(!dem_reader_.loadFile(0,dem_file)){
        ROS_ERROR_STREAM("DEM file reading failed.");
        return false;
    }

    ros::Rate r(10); // 10 hz
    while(!lfm_client_.IsInitialized() && ros::ok()){
        ros::spinOnce(); r.sleep();
    }

    PutLatLonPointsOnGround();

    return true;
}

bool LatLonVisualizer::PutLatLonPointsOnGround(){
    for(size_t i=0; i<pvector_.size(); i++){
        geometry_msgs::Point& p = pvector_[i];
        bool valid = false;
        p.z = dem_reader_.getHeight(p.x,p.y,&valid);
        if(!valid){
            ROS_ERROR_STREAM("Cannot look up height!");
        }

        lfm_client_.MSL2Altitude(p.z,p.z);
        Eigen::Vector3d v;
        ros::Rate loop_rate(10);
        while(!lfm_client_.TransformLatLon2LocalFrame(p.x*DEG2RAD_C,p.y*DEG2RAD_C,p.z,v,frame_)){
            ROS_ERROR_STREAM("Couldn't transform lat lon to "<<frame_);
            ros::spinOnce();
            loop_rate.sleep();
        }
        p.x = v.x(); p.y = v.y(); p.z = v.z();
        ROS_ERROR_STREAM("Obj no::"<<i<<" X::"<<std::setprecision(9)<<p.x<<" Y::"<<std::setprecision(9)<<p.y);
    }

}

void LatLonVisualizer::PublishMarker(){
    visualization_msgs::Marker marker;
    marker.header.frame_id = frame_;
    marker.header.stamp = ros::Time();
    marker.ns = ns_;
    marker.frame_locked = true;
    marker.id = 0;
    marker.type = visualization_msgs::Marker::SPHERE_LIST;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0.0;
    marker.pose.position.y = 0.0;
    marker.pose.position.z = 0.0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = scale_;
    marker.scale.y = scale_;
    marker.scale.z = scale_;
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.points = pvector_;
    marker.colors = cvector_;

    marker_pub_.publish(marker);
}
