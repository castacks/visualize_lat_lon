#include "visualize_lat_lon/latlonvisualizer.h"

int main(int argc, char *argv[]) {
    ros::init(argc, argv, "visualize_lat_lon");
    ros::NodeHandle n("~");
    ros::Rate loop_rate(10);
    LatLonVisualizer llv;
    if(!llv.Initialize(n)){
        ROS_ERROR_STREAM("Failed to Initialize!");
    }

    while(ros::ok()){
        llv.PublishMarker();
        ROS_ERROR_STREAM("Iter");
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
